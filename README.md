Recomended hardware and software for AVAZ web designers.

### Hardware

Good iMacs with budget 1440p screens for secondary usage. Flexirent through Expert electirc could help to spread the cost, I know the owner so could discuss it with him tomorrow.

[iMac with 5k display](https://www.expert.ie/products/apple/imac/imac/27-inch-imac-with-retina-5k-display-3-4ghz-quad-co)

[Flexirent](https://www.expert.ie/online-shopping/flexirent)

[Monitor](http://www.elara.ie/productdetail.aspx?productcode=ECE3350703)

### Software for web

Adobe photoshop, illustrator, and indesign: 734 euro each per year. Marc has everything bar indesign for the next 6 months already so we can discount that much, indesign annual plan is for 294.

[Creative Cloud Pricing](https://www.adobe.com/ie/creativecloud/plans.html?promoid=P3KMQZ9Y&mv=other)

- Photoshop/Lightroom: Photo editing, bulk photo processing.
- Illustrator: Logos and illustrations,
- Indesign: Print layouts like brochures and posters.
- Sketch: Wireframes and UI Design


In total these packages will cost  €2,338.28 / yr

### Software for video people.

Fairly certain this is what anyone would be using, industry standard.

Adobe Premier Pro
Adobe After Effects